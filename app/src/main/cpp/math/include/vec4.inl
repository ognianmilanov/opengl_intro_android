#include <cmath>

constexpr vec4& vec4::at(float* mem) { return *((vec4*)mem); }
constexpr const vec4& vec4::at(const float* mem) { return *((const vec4*)mem); }

constexpr vec4::vec4() : x{}, y{}, z{}, w{} {}
constexpr vec4::vec4(const vec4& other) : x{other.x}, y{other.y}, z{other.z}, w{other.w} {}
constexpr vec4::vec4(float _x, float _y, float _z, float _w) : x{_x}, y{_y}, z{_z}, w{_w} {}
vec4::vec4(std::initializer_list<float> il) : x{*(il.begin() + 0)}, y{*(il.begin() + 1)}, z{*(il.begin() + 2)}, w{*(il.begin() + 3)} {}

vec4& vec4::operator=(const vec4& rhs) { x = rhs.x; y = rhs.y; z = rhs.z; w = rhs.w; return *this; }

float vec4::operator[](int i) const { return v[i]; }
float& vec4::operator[](int i) { return v[i]; }

vec4& vec4::operator+=(const vec4& rhs) { x += rhs.x; y += rhs.y; z += rhs.z; w += rhs.w; return *this; }
vec4& vec4::operator-=(const vec4& rhs) { x -= rhs.x; y -= rhs.y; z -= rhs.z; w -= rhs.w; return *this; }
vec4& vec4::operator*=(const vec4& rhs) { x *= rhs.x; y *= rhs.y; z *= rhs.z; w *= rhs.w; return *this; }
vec4& vec4::operator/=(const vec4& rhs) { x /= rhs.x; y /= rhs.y; z /= rhs.z; w /= rhs.w; return *this; }

vec4& vec4::operator+=(float rhs) { x += rhs; y += rhs; z += rhs; w += rhs; return *this; }
vec4& vec4::operator-=(float rhs) { x -= rhs; y -= rhs; z -= rhs; w -= rhs; return *this; }
vec4& vec4::operator*=(float rhs) { x *= rhs; y *= rhs; z *= rhs; w *= rhs; return *this; }
vec4& vec4::operator/=(float rhs) { x /= rhs; y /= rhs; z /= rhs; w /= rhs; return *this; }

vec4 vec4::operator+(const vec4& rhs) const { return vec4(x + rhs.x, y + rhs.y, z + rhs.z, w + rhs.w); }
vec4 vec4::operator-(const vec4& rhs) const { return vec4(x - rhs.x, y - rhs.y, z - rhs.z, w - rhs.w); }
vec4 vec4::operator*(const vec4& rhs) const { return vec4(x * rhs.x, y * rhs.y, z * rhs.z, w * rhs.w); }
vec4 vec4::operator/(const vec4& rhs) const { return vec4(x / rhs.x, y / rhs.y, z / rhs.z, w / rhs.w); }

vec4 vec4::operator+(float rhs) const { return vec4(x + rhs, y + rhs, z + rhs, w + rhs); }
vec4 vec4::operator-(float rhs) const { return vec4(x - rhs, y - rhs, z - rhs, w - rhs); }
vec4 vec4::operator*(float rhs) const { return vec4(x * rhs, y * rhs, z * rhs, w * rhs); }
vec4 vec4::operator/(float rhs) const { return vec4(x / rhs, y / rhs, z / rhs, w / rhs); }

vec4 vec4::operator-() const { return *this * -1.0f; }

vec4& vec4::set(float _x, float _y, float _z, float _w) { x = _x; y = _y; z = _z; w = _w; return *this; }

float vec4::dot(const vec4& rhs) const { return x * rhs.x + y * rhs.y + z * rhs.z + w * rhs.w; }
float vec4::magnitude_sqr() const { return dot(*this); }
float vec4::magnitude() const { return sqrt(magnitude_sqr()); }
vec4 vec4::normalized() const { vec4 temp{*this}; return temp.normalize(); }
vec4& vec4::normalize() { *this /= magnitude(); return *this; }

vec4 vec4::fromARGB(uint32_t hex) {
    return vec4{((hex >> 16) & 255) / 255.0f, ((hex >> 8) & 255) / 255.0f, (hex & 255) / 255.0f, ((hex >> 24) & 255) / 255.0f};
}

vec4 vec4::fromRGB(uint32_t hex) {
    return vec4{((hex >> 16) & 255) / 255.0f, ((hex >> 8) & 255) / 255.0f, (hex & 255) / 255.0f, 1};
}