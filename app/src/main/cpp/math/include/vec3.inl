#include <cmath>

constexpr vec3& vec3::at(float* mem) { return *((vec3*)mem); }
constexpr const vec3& vec3::at(const float* mem) { return *((const vec3*)mem); }

constexpr vec3::vec3() : x{}, y{}, z{} {}
constexpr vec3::vec3(const vec3& other) : x{other.x}, y{other.y}, z{other.z} {}
constexpr vec3::vec3(float _x, float _y, float _z) : x{_x}, y{_y}, z{_z} {}
vec3::vec3(std::initializer_list<float> il) : x{*(il.begin() + 0)}, y{*(il.begin() + 1)}, z{*(il.begin() + 2)} {}

vec3& vec3::operator=(const vec3& rhs) { x = rhs.x; y = rhs.y; z = rhs.z; return *this; }

float vec3::operator[](int i) const { return v[i]; }
float& vec3::operator[](int i) { return v[i]; }

vec3& vec3::operator+=(const vec3& rhs) { x += rhs.x; y += rhs.y; z += rhs.z; return *this; }
vec3& vec3::operator-=(const vec3& rhs) { x -= rhs.x; y -= rhs.y; z -= rhs.z; return *this; }
vec3& vec3::operator*=(const vec3& rhs) { x *= rhs.x; y *= rhs.y; z *= rhs.z; return *this; }
vec3& vec3::operator/=(const vec3& rhs) { x /= rhs.x; y /= rhs.y; z /= rhs.z; return *this; }

vec3& vec3::operator+=(float rhs) { x += rhs; y += rhs; z += rhs; return *this; }
vec3& vec3::operator-=(float rhs) { x -= rhs; y -= rhs; z -= rhs; return *this; }
vec3& vec3::operator*=(float rhs) { x *= rhs; y *= rhs; z *= rhs; return *this; }
vec3& vec3::operator/=(float rhs) { x /= rhs; y /= rhs; z /= rhs; return *this; }

vec3 vec3::operator+(const vec3& rhs) const { return vec3(x + rhs.x, y + rhs.y, z + rhs.z); }
vec3 vec3::operator-(const vec3& rhs) const { return vec3(x - rhs.x, y - rhs.y, z - rhs.z); }
vec3 vec3::operator*(const vec3& rhs) const { return vec3(x * rhs.x, y * rhs.y, z * rhs.z); }
vec3 vec3::operator/(const vec3& rhs) const { return vec3(x / rhs.x, y / rhs.y, z / rhs.z); }

vec3 vec3::operator+(float rhs) const { return vec3(x + rhs, y + rhs, z + rhs); }
vec3 vec3::operator-(float rhs) const { return vec3(x - rhs, y - rhs, z - rhs); }
vec3 vec3::operator*(float rhs) const { return vec3(x * rhs, y * rhs, z * rhs); }
vec3 vec3::operator/(float rhs) const { return vec3(x / rhs, y / rhs, z / rhs); }

vec3 vec3::operator-() const { return *this * -1.0f; }

vec3& vec3::set(float _x, float _y, float _z) { x = _x; y = _y; z = _z; return *this; }

float vec3::dot(const vec3& rhs) const { return x * rhs.x + y * rhs.y + z * rhs.z; }
float vec3::magnitude_sqr() const { return dot(*this); }
float vec3::magnitude() const { return sqrt(magnitude_sqr()); }
vec3 vec3::normalized() const { vec3 temp{*this}; return temp.normalize(); }
vec3& vec3::normalize() { *this /= magnitude(); return *this; }

vec3 vec3::crossed(const vec3& rhs) const { return vec3(y * rhs.z - rhs.y * z, -(x * rhs.z - rhs.x * z), x * rhs.y - rhs.y * y); }
vec3& vec3::cross(const vec3& rhs) { *this = crossed(rhs); return *this; }