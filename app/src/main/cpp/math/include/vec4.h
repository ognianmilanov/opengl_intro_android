#ifndef MATH_INCLUDE_VEC4_H_
#define MATH_INCLUDE_VEC4_H_

#include <initializer_list>

union vec4 {

    inline constexpr static vec4& at(float* mem);
    inline constexpr static const vec4& at(const float* mem);

    inline constexpr vec4();
    inline constexpr vec4(const vec4& other);
    inline constexpr vec4(float x, float y, float z, float w);
    inline vec4(std::initializer_list<float> il);

    inline vec4& operator=(const vec4& rhs);

    inline float operator[](int i) const;
    inline float& operator[](int i);

    inline vec4& operator+=(const vec4& rhs);
    inline vec4& operator-=(const vec4& rhs);
    inline vec4& operator*=(const vec4& rhs);
    inline vec4& operator/=(const vec4& rhs);

    inline vec4& operator+=(float rhs);
    inline vec4& operator-=(float rhs);
    inline vec4& operator*=(float rhs);
    inline vec4& operator/=(float rhs);

    inline vec4 operator+(const vec4& rhs) const;
    inline vec4 operator-(const vec4& rhs) const;
    inline vec4 operator*(const vec4& rhs) const;
    inline vec4 operator/(const vec4& rhs) const;

    inline vec4 operator+(float rhs) const;
    inline vec4 operator-(float rhs) const;
    inline vec4 operator*(float rhs) const;
    inline vec4 operator/(float rhs) const;

    inline vec4 operator-() const;

    inline vec4& set(float x, float y, float z, float w);

    inline float dot(const vec4& rhs) const;
    inline float magnitude_sqr() const;
    inline float magnitude() const;
    inline vec4 normalized() const;
    inline vec4& normalize();

    inline static vec4 fromARGB(uint32_t hex);
    inline static vec4 fromRGB(uint32_t hex);

    static const vec4 zero;
    static const vec4 one;

    struct {
        float x, y, z, w;
    };
    struct {
        float r, g, b, a;
    };
    float v[4];
};

#include "vec4.inl"

#endif /* MATH_INCLUDE_VEC4_H_ */
