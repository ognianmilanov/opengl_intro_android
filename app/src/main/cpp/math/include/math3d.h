#ifndef MATH_INCLUDE_MATH3D_H_
#define MATH_INCLUDE_MATH3D_H_

#include "vec2.h"
#include "vec3.h"
#include "vec4.h"
#include "quat.h"
#include "mat3x3.h"
#include "mat4x4.h"

inline float deg_to_rad(float a) {
	return a * (M_PI / 180);
}

#endif /* MATH_INCLUDE_MATH3D_H_ */
