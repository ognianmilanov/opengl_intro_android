#include <cmath>

constexpr mat3x3& mat3x3::at(float* mem) { return *((mat3x3*)mem); }
constexpr const mat3x3& mat3x3::at(const float* mem) { return *((const mat3x3*)mem); }

constexpr mat3x3::mat3x3() : m{1, 0, 0, 0, 1, 0, 0, 0, 1} {}

constexpr mat3x3::mat3x3(const mat3x3& o) : m{o.m[0], o.m[1], o.m[2], o.m[3], o.m[4], o.m[5], o.m[6], o.m[7], o.m[8]} {}

mat3x3& mat3x3::operator=(const mat3x3& other) {
    m[0] = other.m[0]; m[1] = other.m[1]; m[2] = other.m[2];
    m[3] = other.m[3]; m[4] = other.m[4]; m[5] = other.m[5];
    m[6] = other.m[6]; m[7] = other.m[7]; m[8] = other.m[8];
    return *this;
}

mat3x3 mat3x3::operator*(const mat3x3& other) const {
    mat3x3 ret;
    ret.m[0] = m[0] * other.m[0] + m[3] * other.m[1] + m[6] * other.m[2];
    ret.m[1] = m[1] * other.m[0] + m[4] * other.m[1] + m[7] * other.m[2];
    ret.m[2] = m[2] * other.m[0] + m[5] * other.m[1] + m[8] * other.m[2];
    ret.m[3] = m[0] * other.m[3] + m[3] * other.m[4] + m[6] * other.m[5];
    ret.m[4] = m[1] * other.m[3] + m[4] * other.m[4] + m[7] * other.m[5];
    ret.m[5] = m[2] * other.m[3] + m[5] * other.m[4] + m[8] * other.m[5];
    ret.m[6] = m[0] * other.m[6] + m[3] * other.m[7] + m[6] * other.m[8];
    ret.m[7] = m[1] * other.m[6] + m[4] * other.m[7] + m[7] * other.m[8];
    ret.m[8] = m[2] * other.m[6] + m[5] * other.m[7] + m[8] * other.m[8];
    return ret;
}

vec3 mat3x3::operator*(const vec3& other) const {
    vec3 ret;
    ret.x = m[0] * other.x + m[3] * other.y + m[6] * other.z;
    ret.y = m[1] * other.x + m[4] * other.y + m[7] * other.z;
    ret.z = m[2] * other.x + m[5] * other.y + m[8] * other.z;
    return ret;
}

void mat3x3::setIdentity() {
    m[0] = 1; m[1] = 0; m[2] = 0;
    m[3] = 0; m[4] = 1; m[5] = 0;
    m[6] = 0; m[7] = 0; m[8] = 1;
}

float mat3x3::getDeterminant() const {
    return m[0] * (m[8] * m[4] - m[5] * m[7])
    - m[1] * (m[8] * m[3] - m[5] * m[6])
    + m[2] * (m[7] * m[3] - m[4] * m[6]);
}

mat3x3 mat3x3::getInverted() const {
    mat3x3 ret;
    
    auto oneDet = 1.0f / getDeterminant();
    
    ret.m[0] = +(m[8] * m[4] - m[5] * m[7]);
    ret.m[1] = -(m[8] * m[1] - m[2] * m[7]);
    ret.m[2] = +(m[5] * m[1] - m[2] * m[4]);
    
    ret.m[3] = -(m[8] * m[3] - m[5] * m[6]);
    ret.m[4] = +(m[8] * m[0] - m[2] * m[6]);
    ret.m[5] = -(m[5] * m[0] - m[2] * m[3]);
    
    ret.m[6] = +(m[7] * m[3] - m[4] * m[6]);
    ret.m[7] = -(m[7] * m[0] - m[1] * m[6]);
    ret.m[8] = +(m[4] * m[0] - m[1] * m[3]);
    
    for (int i = 0; i < 9; ++i) {
        ret.m[i] *= oneDet;
    }
    
    return ret;
}

mat3x3 mat3x3::getTransposed() const {
    mat3x3 ret;

    ret.m[0] = m[0]; ret.m[1] = m[3]; ret.m[2] = m[6];
    ret.m[3] = m[1]; ret.m[4] = m[4]; ret.m[5] = m[7];
    ret.m[6] = m[2]; ret.m[7] = m[5]; ret.m[8] = m[8];

    return ret;
}

void mat3x3::setTranslation(const vec2& t) {
    setIdentity();
    m[6] = t.x;
    m[7] = t.y;
}

void mat3x3::setRotation(float angleRad) {
    setIdentity();
    float c = cos(angleRad);
    float s = sin(angleRad);
    m[0] = c;
    m[1] = -s;
    m[3] = s;
    m[4] = c;
}

void mat3x3::setScale(const vec2& t) {
    setIdentity();
    m[0] = t.x;
    m[4] = t.y;
}

vec2 mat3x3::getTranslation() const {
    return vec2(m[6], m[7]);
}

float mat3x3::getRotation() const {
    return std::fmax(atan2(-m[3], m[0]), atan2(m[1], m[4]));
}

vec2 mat3x3::getScale() const {
    return vec2(m[0], m[4]);
}
