#ifndef MATH_INCLUDE_MAT4X4_H_
#define MATH_INCLUDE_MAT4X4_H_

#include "vec3.h"
#include "vec4.h"

struct mat4x4 {

    inline constexpr static mat4x4& at(float* mem);
    inline constexpr static const mat4x4& at(const float* mem);

    inline constexpr mat4x4();

    inline constexpr mat4x4(const mat4x4& other);

    inline mat4x4& operator=(const mat4x4& other);

    inline mat4x4 operator*(const mat4x4& other) const;

    inline vec4 operator*(const vec4& other) const;

    inline mat4x4& operator*=(const mat4x4& other);

    inline void setIdentity();

    inline mat4x4 getInverted() const;

    inline void frustum(float left, float right, float bottom, float top, float near, float far);

    inline void perspective(float fovy, float aspect, float zNear, float zFar);

    inline void translate(const vec3& amount);

    inline void rotate(float angle, const vec3& axis);

    float m[16];
};

#include "mat4x4.inl"

#endif /* MATH_INCLUDE_MAT4X4_H_ */
