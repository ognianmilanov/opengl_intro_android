#ifndef MATH_INCLUDE_VEC2_H_
#define MATH_INCLUDE_VEC2_H_

#include <initializer_list>

union vec2 {

    inline constexpr static vec2& at(float* mem);
    inline constexpr static const vec2& at(const float* mem);

    inline constexpr vec2();
    inline constexpr vec2(const vec2& other);
    inline constexpr vec2(float x, float y);
    inline vec2(std::initializer_list<float> il);

    inline vec2& operator=(const vec2& rhs);

    inline float operator[](int i) const;
    inline float& operator[](int i);

    inline vec2& operator+=(const vec2& rhs);
    inline vec2& operator-=(const vec2& rhs);
    inline vec2& operator*=(const vec2& rhs);
    inline vec2& operator/=(const vec2& rhs);

    inline vec2& operator+=(float rhs);
    inline vec2& operator-=(float rhs);
    inline vec2& operator*=(float rhs);
    inline vec2& operator/=(float rhs);

    inline vec2 operator+(const vec2& rhs) const;
    inline vec2 operator-(const vec2& rhs) const;
    inline vec2 operator*(const vec2& rhs) const;
    inline vec2 operator/(const vec2& rhs) const;

    inline vec2 operator+(float rhs) const;
    inline vec2 operator-(float rhs) const;
    inline vec2 operator*(float rhs) const;
    inline vec2 operator/(float rhs) const;

    inline vec2 operator-() const;

    inline vec2& set(float x, float y);

    inline float dot(const vec2& rhs) const;
    inline float magnitude_sqr() const;
    inline float magnitude() const;
    inline vec2 normalized() const;
    inline vec2& normalize();

    static const vec2 zero;
    static const vec2 one;
    static const vec2 right;
    static const vec2 up;
    static const vec2 left;
    static const vec2 down;

    struct {
        float x, y;
    };
    float v[2];
};

#include "vec2.inl"

#endif /* MATH_INCLUDE_VEC2_H_ */
