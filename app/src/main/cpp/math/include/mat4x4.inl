#include <cmath>

constexpr mat4x4& mat4x4::at(float* mem) { return *((mat4x4*)mem); }
constexpr const mat4x4& mat4x4::at(const float* mem) { return *((const mat4x4*)mem); }

constexpr mat4x4::mat4x4() :
	m{1, 0, 0, 0,
	  0, 1, 0, 0,
	  0, 0, 1, 0,
	  0, 0, 0, 1} {}

constexpr mat4x4::mat4x4(const mat4x4& o) : 
	m{o.m[ 0], o.m[ 1], o.m[ 2], o.m[ 3],
	  o.m[ 4], o.m[ 5], o.m[ 6], o.m[ 7],
	  o.m[ 8], o.m[ 9], o.m[10], o.m[11],
	  o.m[12], o.m[13], o.m[14], o.m[15]} {}

mat4x4& mat4x4::operator=(const mat4x4& other) {
    m[ 0] = other.m[ 0]; m[ 1] = other.m[ 1]; m[ 2] = other.m[ 2]; m[ 3] = other.m[ 3];
    m[ 4] = other.m[ 4]; m[ 5] = other.m[ 5]; m[ 6] = other.m[ 6]; m[ 7] = other.m[ 7];
    m[ 8] = other.m[ 8]; m[ 9] = other.m[ 9]; m[10] = other.m[10]; m[11] = other.m[11];
    m[12] = other.m[12]; m[13] = other.m[13]; m[14] = other.m[14]; m[15] = other.m[15];
    return *this;
}

mat4x4 mat4x4::operator*(const mat4x4& other) const {
    mat4x4 ret;
	ret.m[ 0] = m[ 0] * other.m[ 0] + m[ 4] * other.m[ 1] + m[ 8] * other.m[ 2] + m[12] * other.m[ 3];
	ret.m[ 1] = m[ 1] * other.m[ 0] + m[ 5] * other.m[ 1] + m[ 9] * other.m[ 2] + m[13] * other.m[ 3];
	ret.m[ 2] = m[ 2] * other.m[ 0] + m[ 6] * other.m[ 1] + m[10] * other.m[ 2] + m[14] * other.m[ 3];
	ret.m[ 3] = m[ 3] * other.m[ 0] + m[ 7] * other.m[ 1] + m[11] * other.m[ 2] + m[15] * other.m[ 3];
	ret.m[ 4] = m[ 0] * other.m[ 4] + m[ 4] * other.m[ 5] + m[ 8] * other.m[ 6] + m[12] * other.m[ 7];
	ret.m[ 5] = m[ 1] * other.m[ 4] + m[ 5] * other.m[ 5] + m[ 9] * other.m[ 6] + m[13] * other.m[ 7];
	ret.m[ 6] = m[ 2] * other.m[ 4] + m[ 6] * other.m[ 5] + m[10] * other.m[ 6] + m[14] * other.m[ 7];
	ret.m[ 7] = m[ 3] * other.m[ 4] + m[ 7] * other.m[ 5] + m[11] * other.m[ 6] + m[15] * other.m[ 7];
	ret.m[ 8] = m[ 0] * other.m[ 8] + m[ 4] * other.m[ 9] + m[ 8] * other.m[10] + m[12] * other.m[11];
	ret.m[ 9] = m[ 1] * other.m[ 8] + m[ 5] * other.m[ 9] + m[ 9] * other.m[10] + m[13] * other.m[11];
	ret.m[10] = m[ 2] * other.m[ 8] + m[ 6] * other.m[ 9] + m[10] * other.m[10] + m[14] * other.m[11];
	ret.m[11] = m[ 3] * other.m[ 8] + m[ 7] * other.m[ 9] + m[11] * other.m[10] + m[15] * other.m[11];
	ret.m[12] = m[ 0] * other.m[12] + m[ 4] * other.m[13] + m[ 8] * other.m[14] + m[12] * other.m[15];
	ret.m[13] = m[ 1] * other.m[12] + m[ 5] * other.m[13] + m[ 9] * other.m[14] + m[13] * other.m[15];
	ret.m[14] = m[ 2] * other.m[12] + m[ 6] * other.m[13] + m[10] * other.m[14] + m[14] * other.m[15];
	ret.m[15] = m[ 3] * other.m[12] + m[ 7] * other.m[13] + m[11] * other.m[14] + m[15] * other.m[15];
    return ret;
}

vec4 mat4x4::operator*(const vec4& other) const {
    vec4 ret;
	ret.x = m[ 0] * other.x + m[ 4] * other.y + m[ 8] * other.z + m[12] * other.w;
	ret.y = m[ 1] * other.x + m[ 5] * other.y + m[ 9] * other.z + m[13] * other.w;
	ret.z = m[ 2] * other.x + m[ 6] * other.y + m[10] * other.z + m[14] * other.w;
	ret.w = m[ 3] * other.x + m[ 7] * other.y + m[11] * other.z + m[15] * other.w;
    return ret;
}

mat4x4& mat4x4::operator*=(const mat4x4& other) {
	operator=(*this * other);
	return *this;
}

void mat4x4::setIdentity() {
    m[ 0] = 1; m[ 1] = 0; m[ 2] = 0; m[ 3] = 0;
    m[ 4] = 0; m[ 5] = 1; m[ 6] = 0; m[ 7] = 0;
    m[ 8] = 0; m[ 9] = 0; m[10] = 1; m[11] = 0;
    m[12] = 0; m[13] = 0; m[14] = 0; m[15] = 1;
}

mat4x4 mat4x4::getInverted() const {
    mat4x4 ret;
    
	ret.m[ 0] =
		 m[ 5] * m[10] * m[15] - 
		 m[ 5] * m[11] * m[14] - 
		 m[ 9] * m[ 6] * m[15] + 
		 m[ 9] * m[ 7] * m[14] +
		 m[13] * m[ 6] * m[11] - 
		 m[13] * m[ 7] * m[10];
	
	ret.m[ 4] =
		-m[ 4] * m[10] * m[15] + 
		 m[ 4] * m[11] * m[14] + 
		 m[ 8] * m[ 6] * m[15] - 
		 m[ 8] * m[ 7] * m[14] - 
		 m[12] * m[ 6] * m[11] + 
		 m[12] * m[ 7] * m[10];
	
	ret.m[ 8] =
		 m[ 4] * m[ 9] * m[15] - 
		 m[ 4] * m[11] * m[13] - 
		 m[ 8] * m[ 5] * m[15] + 
		 m[ 8] * m[ 7] * m[13] + 
		 m[12] * m[ 5] * m[11] - 
		 m[12] * m[ 7] * m[ 9];
	
	ret.m[12] =
		-m[ 4] * m[ 9] * m[14] + 
		 m[ 4] * m[10] * m[13] +
		 m[ 8] * m[ 5] * m[14] - 
		 m[ 8] * m[ 6] * m[13] - 
		 m[12] * m[ 5] * m[10] + 
		 m[12] * m[ 6] * m[ 9];
	
	ret.m[ 1] =
		-m[ 1] * m[10] * m[15] + 
		 m[ 1] * m[11] * m[14] + 
		 m[ 9] * m[ 2] * m[15] - 
		 m[ 9] * m[ 3] * m[14] - 
		 m[13] * m[ 2] * m[11] + 
		 m[13] * m[ 3] * m[10];
	
	ret.m[ 5] =
		 m[ 0] * m[10] * m[15] - 
		 m[ 0] * m[11] * m[14] - 
		 m[ 8] * m[ 2] * m[15] + 
		 m[ 8] * m[ 3] * m[14] + 
		 m[12] * m[ 2] * m[11] - 
		 m[12] * m[ 3] * m[10];
	
	ret.m[ 9] =
		-m[ 0] * m[ 9] * m[15] + 
		 m[ 0] * m[11] * m[13] + 
		 m[ 8] * m[ 1] * m[15] - 
		 m[ 8] * m[ 3] * m[13] - 
		 m[12] * m[ 1] * m[11] + 
		 m[12] * m[ 3] * m[ 9];
	
	ret.m[13] =
		 m[ 0] * m[ 9] * m[14] - 
		 m[ 0] * m[10] * m[13] - 
		 m[ 8] * m[ 1] * m[14] + 
		 m[ 8] * m[ 2] * m[13] + 
		 m[12] * m[ 1] * m[10] - 
		 m[12] * m[ 2] * m[ 9];
	
	ret.m[ 2] =
		 m[ 1] * m[ 6] * m[15] - 
		 m[ 1] * m[ 7] * m[14] - 
		 m[ 5] * m[ 2] * m[15] + 
		 m[ 5] * m[ 3] * m[14] + 
		 m[13] * m[ 2] * m[ 7] - 
		 m[13] * m[ 3] * m[ 6];
	
	ret.m[ 6] =
		-m[ 0] * m[ 6] * m[15] + 
		 m[ 0] * m[ 7] * m[14] + 
		 m[ 4] * m[ 2] * m[15] - 
		 m[ 4] * m[ 3] * m[14] - 
		 m[12] * m[ 2] * m[ 7] + 
		 m[12] * m[ 3] * m[ 6];
	
	ret.m[10] =
		 m[ 0] * m[ 5] * m[15] - 
		 m[ 0] * m[ 7] * m[13] - 
		 m[ 4] * m[ 1] * m[15] + 
		 m[ 4] * m[ 3] * m[13] + 
		 m[12] * m[ 1] * m[ 7] - 
		 m[12] * m[ 3] * m[ 5];
	
	ret.m[14] =
		-m[ 0] * m[ 5] * m[14] + 
		 m[ 0] * m[ 6] * m[13] + 
		 m[ 4] * m[ 1] * m[14] - 
		 m[ 4] * m[ 2] * m[13] - 
		 m[12] * m[ 1] * m[ 6] + 
		 m[12] * m[ 2] * m[ 5];
	
	ret.m[ 3] =
		-m[ 1] * m[ 6] * m[11] + 
		 m[ 1] * m[ 7] * m[10] + 
		 m[ 5] * m[ 2] * m[11] - 
		 m[ 5] * m[ 3] * m[10] - 
		 m[ 9] * m[ 2] * m[ 7] + 
		 m[ 9] * m[ 3] * m[ 6];
	
	ret.m[ 7] =
		 m[ 0] * m[ 6] * m[11] - 
		 m[ 0] * m[ 7] * m[10] - 
		 m[ 4] * m[ 2] * m[11] + 
		 m[ 4] * m[ 3] * m[10] + 
		 m[ 8] * m[ 2] * m[ 7] - 
		 m[ 8] * m[ 3] * m[ 6];
	
	ret.m[11] =
		-m[ 0] * m[ 5] * m[11] + 
		 m[ 0] * m[ 7] * m[ 9] + 
		 m[ 4] * m[ 1] * m[11] - 
		 m[ 4] * m[ 3] * m[ 9] - 
		 m[ 8] * m[ 1] * m[ 7] + 
		 m[ 8] * m[ 3] * m[ 5];
	
	ret.m[15] =
		 m[ 0] * m[ 5] * m[10] - 
		 m[ 0] * m[ 6] * m[ 9] - 
		 m[ 4] * m[ 1] * m[10] + 
		 m[ 4] * m[ 2] * m[ 9] + 
		 m[ 8] * m[ 1] * m[ 6] - 
		 m[ 8] * m[ 2] * m[ 5];
	
	float det = m[ 0] * ret.m[ 0] + m[ 1] * ret.m[ 4] + m[ 2] * ret.m[ 8] + m[ 3] * ret.m[12];
	
	for (int i = 0; i < 16; ++i) {
		ret.m[i] /= det;
	}
    
    return ret;
}

inline void mat4x4::frustum(float left, float right, float bottom, float top, float near, float far) {
	const auto r_width  = 1.0f / (right - left);
	const auto r_height = 1.0f / (top - bottom);
	const auto r_depth  = 1.0f / (near - far);
	const auto x = 2.0f * (near * r_width);
	const auto y = 2.0f * (near * r_height);
	const auto A = (right + left) * r_width;
	const auto B = (top + bottom) * r_height;
	const auto C = (far + near) * r_depth;
	const auto D = 2.0f * (far * near * r_depth);
	m[ 0] = x;
	m[ 5] = y;
	m[ 8] = A;
	m[ 9] = B;
	m[10] = C;
	m[14] = D;
	m[11] = -1.0f;
	m[ 1] = 0.0f;
	m[ 2] = 0.0f;
	m[ 3] = 0.0f;
	m[ 4] = 0.0f;
	m[ 6] = 0.0f;
	m[ 7] = 0.0f;
	m[12] = 0.0f;
	m[13] = 0.0f;
	m[15] = 0.0f;
}

void mat4x4::perspective(float fovy, float aspect, float zNear, float zFar) {
	const float f = 1.0f / tan(fovy);
	const float dist = 1.0f / (zNear - zFar);
	m[ 0] = f / aspect;
	m[ 1] = 0.0f;
	m[ 2] = 0.0f;
	m[ 3] = 0.0f;
	m[ 4] = 0.0f;
	m[ 5] = f;
	m[ 6] = 0.0f;
	m[ 7] = 0.0f;
	m[ 8] = 0.0f;
	m[ 9] = 0.0f;
	m[10] = (zFar + zNear) * dist;
	m[11] = -1.0f;
	m[12] = 0.0f;
	m[13] = 0.0f;
	m[14] = 2.0f * zFar * zNear * dist;
	m[15] = 0.0f;
}

void mat4x4::translate(const vec3& amount) {
	for (int i = 0 ; i < 4 ; ++i) {
        m[12 + i] += m[i] * amount.x + m[4 + i] * amount.y + m[8 + i] * amount.z;
    }
}

void mat4x4::rotate(float angle, const vec3& axis) {
	m[ 3] = 0;
	m[ 7] = 0;
	m[11] = 0;
	m[12] = 0;
	m[13] = 0;
	m[14] = 0;
	m[15] = 1;
	const float s = sin(angle);
	const float c = cos(angle);
	const float nc = 1 - c;
	const float xy = axis.x * axis.y;
	const float yz = axis.y * axis.z;
	const float zx = axis.z * axis.x;
	const float xs = axis.x * s;
	const float ys = axis.y * s;
	const float zs = axis.z * s;
	m[ 0] = axis.x * axis.x * nc + c;
	m[ 4] = xy * nc - zs;
	m[ 8] = zx * nc + ys;
	m[ 1] = xy * nc + zs;
	m[ 5] = axis.y * axis.y * nc + c;
	m[ 9] = yz * nc - xs;
	m[ 2] = zx * nc - ys;
	m[ 6] = yz * nc + xs;
	m[10] = axis.z * axis.z * nc + c;
}