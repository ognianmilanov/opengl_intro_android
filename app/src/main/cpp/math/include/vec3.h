#ifndef MATH_INCLUDE_VEC3_H_
#define MATH_INCLUDE_VEC3_H_

#include <initializer_list>

union vec3 {

    inline constexpr static vec3& at(float* mem);
    inline constexpr static const vec3& at(const float* mem);

    inline constexpr vec3();
    inline constexpr vec3(const vec3& other);
    inline constexpr vec3(float x, float y, float z);
    inline vec3(std::initializer_list<float> il);

    inline vec3& operator=(const vec3& rhs);

    inline float operator[](int i) const;
    inline float& operator[](int i);

    inline vec3& operator+=(const vec3& rhs);
    inline vec3& operator-=(const vec3& rhs);
    inline vec3& operator*=(const vec3& rhs);
    inline vec3& operator/=(const vec3& rhs);

    inline vec3& operator+=(float rhs);
    inline vec3& operator-=(float rhs);
    inline vec3& operator*=(float rhs);
    inline vec3& operator/=(float rhs);

    inline vec3 operator+(const vec3& rhs) const;
    inline vec3 operator-(const vec3& rhs) const;
    inline vec3 operator*(const vec3& rhs) const;
    inline vec3 operator/(const vec3& rhs) const;

    inline vec3 operator+(float rhs) const;
    inline vec3 operator-(float rhs) const;
    inline vec3 operator*(float rhs) const;
    inline vec3 operator/(float rhs) const;

    inline vec3 operator-() const;

    inline vec3& set(float x, float y, float z);

    inline float dot(const vec3& rhs) const;
    inline float magnitude_sqr() const;
    inline float magnitude() const;
    inline vec3 normalized() const;
    inline vec3& normalize();

    inline vec3 crossed(const vec3& rhs) const;
    inline vec3& cross(const vec3& rhs);

    static const vec3 zero;
    static const vec3 one;
    static const vec3 right;
    static const vec3 up;
    static const vec3 left;
    static const vec3 down;
    static const vec3 forward;
    static const vec3 back;

    struct {
        float x, y, z;
    };
    struct {
        float r, g, b;
    };
    float v[3];
};

#include "vec3.inl"

#endif /* MATH_INCLUDE_VEC3_H_ */
