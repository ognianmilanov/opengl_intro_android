#include <cmath>

constexpr vec2& vec2::at(float* mem) { return *((vec2*)mem); }
constexpr const vec2& vec2::at(const float* mem) { return *((const vec2*)mem); }

constexpr vec2::vec2() : x{}, y{} {}
constexpr vec2::vec2(const vec2& other) : x{other.x}, y{other.y} {}
constexpr vec2::vec2(float _x, float _y) : x{_x}, y{_y} {}
vec2::vec2(std::initializer_list<float> il) : x{*(il.begin() + 0)}, y{*(il.begin() + 1)} {}

vec2& vec2::operator=(const vec2& rhs) { x = rhs.x; y = rhs.y; return *this; }

float vec2::operator[](int i) const { return v[i]; }
float& vec2::operator[](int i) { return v[i]; }

vec2& vec2::operator+=(const vec2& rhs) { x += rhs.x; y += rhs.y; return *this; }
vec2& vec2::operator-=(const vec2& rhs) { x -= rhs.x; y -= rhs.y; return *this; }
vec2& vec2::operator*=(const vec2& rhs) { x *= rhs.x; y *= rhs.y; return *this; }
vec2& vec2::operator/=(const vec2& rhs) { x /= rhs.x; y /= rhs.y; return *this; }

vec2& vec2::operator+=(float rhs) { x += rhs; y += rhs; return *this; }
vec2& vec2::operator-=(float rhs) { x -= rhs; y -= rhs; return *this; }
vec2& vec2::operator*=(float rhs) { x *= rhs; y *= rhs; return *this; }
vec2& vec2::operator/=(float rhs) { x /= rhs; y /= rhs; return *this; }

vec2 vec2::operator+(const vec2& rhs) const { return vec2(x + rhs.x, y + rhs.y); }
vec2 vec2::operator-(const vec2& rhs) const { return vec2(x - rhs.x, y - rhs.y); }
vec2 vec2::operator*(const vec2& rhs) const { return vec2(x * rhs.x, y * rhs.y); }
vec2 vec2::operator/(const vec2& rhs) const { return vec2(x / rhs.x, y / rhs.y); }

vec2 vec2::operator+(float rhs) const { return vec2(x + rhs, y + rhs); }
vec2 vec2::operator-(float rhs) const { return vec2(x - rhs, y - rhs); }
vec2 vec2::operator*(float rhs) const { return vec2(x * rhs, y * rhs); }
vec2 vec2::operator/(float rhs) const { return vec2(x / rhs, y / rhs); }

vec2 vec2::operator-() const { return *this * -1.0f; }

vec2& vec2::set(float _x, float _y) { x = _x; y = _y; return *this; }

float vec2::dot(const vec2& rhs) const { return x * rhs.x + y * rhs.y; }
float vec2::magnitude_sqr() const { return dot(*this); }
float vec2::magnitude() const { return sqrt(magnitude_sqr()); }
vec2 vec2::normalized() const { vec2 temp{*this}; return temp.normalize(); }
vec2& vec2::normalize() { *this /= magnitude(); return *this; }