#ifndef MATH_INCLUDE_MAT3X3_H_
#define MATH_INCLUDE_MAT3X3_H_

#include "vec2.h"
#include "vec3.h"

struct mat3x3 {

    inline constexpr static mat3x3& at(float* mem);
    inline constexpr static const mat3x3& at(const float* mem);

    inline constexpr mat3x3();

    inline constexpr mat3x3(const mat3x3& other);

    inline mat3x3& operator=(const mat3x3& other);

    inline mat3x3 operator*(const mat3x3& other) const;

    inline vec3 operator*(const vec3& other) const;

    inline void setIdentity();

    inline float getDeterminant() const;

    inline mat3x3 getInverted() const;

    inline mat3x3 getTransposed() const;

    inline void setTranslation(const vec2& t);
    inline void setRotation(float angleRad);
    inline void setScale(const vec2& s);

    inline vec2 getTranslation() const;
    inline float getRotation() const;
    inline vec2 getScale() const;

    float m[9];
};

#include "mat3x3.inl"

#endif /* MATH_INCLUDE_MAT3X3_H_ */
