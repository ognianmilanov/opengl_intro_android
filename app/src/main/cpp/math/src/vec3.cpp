#include "vec3.h"

const vec3 vec3::zero{0, 0, 0};
const vec3 vec3::one{1, 1, 1};
const vec3 vec3::right{1, 0, 0};
const vec3 vec3::up{0, 1, 0};
const vec3 vec3::left{-1, 0, 0};
const vec3 vec3::down{0, -1, 0};
const vec3 vec3::forward{0, 0, 1};
const vec3 vec3::back{0, 0, -1};
