LOCAL_PATH := $(call my-dir)

##########################################################

LOCAL_MODULE := png_static

LOCAL_SRC_FILES := \
    external/libpng/png.c \
    external/libpng/pngerror.c \
    external/libpng/pngget.c \
    external/libpng/pngmem.c \
    external/libpng/pngpread.c \
    external/libpng/pngread.c \
    external/libpng/pngrio.c \
    external/libpng/pngrtran.c \
    external/libpng/pngrutil.c \
    external/libpng/pngset.c \
    external/libpng/pngtrans.c \
    external/libpng/pngwio.c \
    external/libpng/pngwrite.c \
    external/libpng/pngwtran.c \
    external/libpng/pngwutil.c \

LOCAL_SRC_FILES_arm := \
	external/libpng/arm/arm_init.c.neon \
	external/libpng/arm/filter_neon.S.neon \
	external/libpng/arm/filter_neon_intrinsics.c.neon

LOCAL_CFLAGS += -ftrapv
LOCAL_CFLAGS_arm += -DPNG_ARM_NEON_OPT=2
LOCAL_CFLAGS_x86 += -DPNG_INTEL_SSE_OPT=1

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/external/libpng

LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/external/libpng

LOCAL_LDLIBS := -lz
include $(BUILD_STATIC_LIBRARY)

##########################################################

include $(CLEAR_VARS)
LOCAL_MODULE := OpenGL_intro

LOCAL_SRC_FILES := \
    android_native_app_glue.cpp \
	native-lib.cpp \
	math/src/vec3.cpp \
    src/assets_util.cpp \
    src/gles2util.cpp \
    renderers/src/renderer_simple.cpp \
    renderers/src/renderer_sphere.cpp \
    renderers/src/renderer_stars.cpp \

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/include \
	$(LOCAL_PATH)/math/include \
	$(LOCAL_PATH)/renderers/include

LOCAL_CFLAGS :=

LOCAL_LDLIBS := -llog -lm -lEGL -lGLESv2 -landroid -lz
LOCAL_SHARED_LIBRARIES +=
LOCAL_STATIC_LIBRARIES += png_static
include $(BUILD_SHARED_LIBRARY)