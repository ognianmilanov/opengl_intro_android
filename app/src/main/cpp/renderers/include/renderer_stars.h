#ifndef RENDERERS_INCLUDE_RENDERER_STARS_H_
#define RENDERERS_INCLUDE_RENDERER_STARS_H_

#include "renderer.h"

#include "gles2util.h"
#include "math3d.h"

class renderer_stars : public renderer {
public:

	renderer_stars();

	virtual ~renderer_stars();

	virtual void create(void* userData = nullptr) override;

	virtual void resize(unsigned width, unsigned height) override;

	virtual void draw() override;

	virtual void destroy() override;

private:
	program_ptr m_program;
	buffer_ptr m_geometry;
	buffer_ptr m_indices;
	unsigned m_numVertices;
	unsigned m_numIndices;
	GLsizei m_geomStride;
	GLint m_uniformMVP;
	GLint m_uniformTime;
	GLint m_attributePosition;
	GLint m_attributeSize;
	GLint m_attributeColor;
	GLint m_attributeBlink;
	float m_time;
	mat4x4 m_matModel;
	mat4x4 m_matView;
	mat4x4 m_matProjection;
};

#endif /* RENDERERS_INCLUDE_RENDERER_STARS_H_ */
