#ifndef RENDERERS_INCLUDE_RENDERER_SIMPLE_H_
#define RENDERERS_INCLUDE_RENDERER_SIMPLE_H_

#include "renderer.h"

#include "gles2util.h"
#include "math3d.h"

class renderer_simple : public renderer {
public:

	renderer_simple();

	virtual ~renderer_simple();

	virtual void create(void* userData = nullptr) override;

	virtual void resize(unsigned width, unsigned height) override;

	virtual void draw() override;

	virtual void destroy() override;

private:
	program_ptr m_program;
	buffer_ptr m_geometry;
	GLint m_attributePosition;
};

#endif /* RENDERERS_INCLUDE_RENDERER_SIMPLE_H_ */
