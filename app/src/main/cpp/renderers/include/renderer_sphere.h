#ifndef RENDERERS_INCLUDE_RENDERER_SPHERE_H_
#define RENDERERS_INCLUDE_RENDERER_SPHERE_H_

#include "renderer.h"

#include "assets_util.h"
#include "math3d.h"

class renderer_sphere : public renderer {
public:

	renderer_sphere();

	virtual ~renderer_sphere();

	virtual void create(void* userData = nullptr) override;

	virtual void resize(unsigned width, unsigned height) override;

	virtual void draw() override;

	virtual void destroy() override;

private:
	program_ptr m_program;
	buffer_ptr m_geometry;
	buffer_ptr m_indices;
	unsigned m_numVertices;
	unsigned m_numIndices;
	GLsizei m_geomStride;
	GLint m_attributePosition;
	GLint m_attributeNormal;
	GLint m_attributeTexCoord;
	GLint m_uniformMV;
	GLint m_uniformMVP;
	GLint m_uniformTexture;
	GLint m_uniformLightPos;
	mat4x4 m_matModel;
	mat4x4 m_matView;
	mat4x4 m_matProjection;
	vec3 m_lightPos;
	texture_ptr m_texture;
	float m_time = 0;
};

#endif /* RENDERERS_INCLUDE_RENDERER_SPHERE_H_ */
