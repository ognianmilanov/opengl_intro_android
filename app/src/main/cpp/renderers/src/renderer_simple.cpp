#include "renderer_simple.h"

#include "assets_util.h"

renderer_simple::renderer_simple() :
	renderer{},
	m_program{},
	m_geometry{},
	m_attributePosition{-1} {
}

renderer_simple::~renderer_simple() {
}

void renderer_simple::create(void* userData) {
#ifdef ANDROID
	m_program = link_program(load_text_asset(static_cast<AAssetManager*>(userData), std::string("shaders/simple_vert")),
							 load_text_asset(static_cast<AAssetManager*>(userData), std::string("shaders/simple_frag")));
#else //ANDROID
	m_program = link_program(load_text_file(std::string("assets/shaders/simple_vert")), load_text_file(std::string("assets/shaders/simple_frag")));
#endif //ANDROID

	m_attributePosition = get_attribute_location(m_program, std::string("a_position"));

	static const GLfloat geometry[] {
		-0.5f, -0.5f, 0,
		 0.5f, -0.5f, 0,
		 0.0f,  0.5f, 0
	};

	m_geometry = make_array_buffer(sizeof(geometry), geometry, GL_STATIC_DRAW);

	glClearColor(0.4, 0.4, 0.4, 0.0); glThrow();
}

void renderer_simple::resize(unsigned width, unsigned height) {
	glViewport(0, 0, width, height); glThrow();
}

void renderer_simple::draw() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); glThrow();

	glUseProgram(*m_program); glThrow();

    glBindBuffer(GL_ARRAY_BUFFER, *m_geometry); glThrow();

    glVertexAttribPointer(m_attributePosition, 3, GL_FLOAT, GL_FALSE, 0, nullptr); glThrow();

    glEnableVertexAttribArray(m_attributePosition); glThrow();

    glDrawArrays(GL_TRIANGLES, 0, 3); glThrow();

    glDisableVertexAttribArray(m_attributePosition); glThrow();

    glBindBuffer(GL_ARRAY_BUFFER, 0); glThrow();
}

void renderer_simple::destroy() {
	m_attributePosition = -1;
	m_program = nullptr;
	m_geometry = nullptr;
}
