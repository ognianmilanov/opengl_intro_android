#include "renderer_sphere.h"

#include <vector>

#include "assets_util.h"

#include "logging.h"

renderer_sphere::renderer_sphere() :
	renderer{},
	m_program{},
	m_geometry{},
	m_indices{},
	m_numVertices{},
	m_numIndices{},
	m_geomStride{},
	m_attributePosition{-1},
	m_attributeNormal{-1},
	m_attributeTexCoord{-1},
	m_uniformMV{-1},
	m_uniformMVP{-1},
	m_uniformTexture{-1},
	m_uniformLightPos{-1},
	m_matModel{},
	m_matView{},
	m_matProjection{},
	m_lightPos{},
	m_texture{},
	m_time{} {
}

renderer_sphere::~renderer_sphere() {
}

void renderer_sphere::create(void* userData) {
#ifdef ANDROID
	m_program = link_program(load_text_asset(static_cast<AAssetManager*>(userData), std::string("shaders/sphere_vert")),
							 load_text_asset(static_cast<AAssetManager*>(userData), std::string("shaders/sphere_frag")));
#else //ANDROID
	m_program = link_program(load_text_file(std::string("assets/shaders/sphere_vert")), load_text_file(std::string("assets/shaders/sphere_frag")));
#endif //ANDROID
	m_uniformMV = get_uniform_location(m_program, "u_mv");
	m_uniformMVP = get_uniform_location(m_program, "u_mvp");
	m_uniformTexture = get_uniform_location(m_program, "u_texture");
	m_uniformLightPos = get_uniform_location(m_program, "u_light_pos");
	m_attributePosition = get_attribute_location(m_program, "a_position");
	m_attributeNormal = get_attribute_location(m_program, "a_normal");
	m_attributeTexCoord = get_attribute_location(m_program, "a_tex_coord");

	unsigned texWidth{}, texHeight{};
	GLint texFormat{};
#ifdef ANDROID
	m_texture = load_png_texture(static_cast<AAssetManager*>(userData), "textures/earth.png", texWidth, texHeight, texFormat);
#else //ANDROID
	m_texture = load_png_texture("assets/textures/earth.png", texWidth, texHeight, texFormat);
#endif //ANDROID

	struct Attribs {
		float x, y, z;
		vec3 normals;
		float s, t;
	};

	std::vector<Attribs> geometry;

	const int numX = 100;
	const int numY = 100;
	const float stepX = (2.0f * M_PI) / static_cast<float>(numX - 1);
	const float stepY = M_PI / static_cast<float>(numY - 1);
	for (int y = 0; y < numY; ++y) {
		const float ay = -0.5f * M_PI + y * stepY;
		for (int x = 0; x < numX; ++x) {
			const float ax = x * stepX;
			Attribs attribs;
			attribs.x = cos(ay) * cos(ax);
			attribs.y = cos(ay) * sin(ax);
			attribs.z = sin(ay);

			attribs.normals = vec3(attribs.x, attribs.y, attribs.z).normalized();

			attribs.s = ax * (0.5f / M_PI);
			attribs.t = 0.5f + (ay * (1.0f / M_PI));

			if (attribs.t < 0) attribs.t = 0;

			geometry.push_back(attribs);
		}
	}

	m_geometry = make_array_buffer(geometry.size() * sizeof(Attribs), geometry.data(), GL_STATIC_DRAW);
	m_numVertices = geometry.size();
	m_geomStride = sizeof(Attribs);

	std::vector<GLushort> indices;

	for (unsigned i = 0; i < m_numVertices - numX - 1; ++i) {
		indices.push_back(i);
		indices.push_back(i + numX + 1);
		indices.push_back(i + numX);

		indices.push_back(i);
		indices.push_back(i + 1);
		indices.push_back(i + numX + 1);
	}

	m_indices = make_element_array_buffer(indices.size() * sizeof(GLushort), indices.data(), GL_STATIC_DRAW);
	m_numIndices = indices.size();

	m_matView.translate(vec3::back * 1.2f);

	glEnable(GL_CULL_FACE); glThrow();
	glCullFace(GL_BACK); glThrow();
	glFrontFace(GL_CCW); glThrow();
	glEnable(GL_DEPTH_TEST); glThrow();

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); glThrow();
}

void renderer_sphere::resize(unsigned width, unsigned height) {
	glViewport(0, 0, width, height); glThrow();

	const float aspect = static_cast<float>(width) / height;

	m_matProjection.perspective(deg_to_rad(60), aspect, 0.1f, 5.0f);
}

void renderer_sphere::draw() {

	m_time += 16.0f/1000;

	mat4x4 rot;
	rot.rotate(deg_to_rad(0.3f), vec3::up.normalized());
	m_matModel *= rot;
	rot.rotate(deg_to_rad(0.1f), vec3::right.normalized());
	m_matModel *= rot;

	m_lightPos.z = -sin(m_time * 3) * 2.2f + 1.6f;

	mat4x4 mv = m_matView * m_matModel;
	mat4x4 mvp = m_matProjection * mv;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); glThrow();

	glUseProgram(*m_program); glThrow();
	glUniformMatrix4fv(m_uniformMV, 1, GL_FALSE, mv.m); glThrow();
	glUniformMatrix4fv(m_uniformMVP, 1, GL_FALSE, mvp.m); glThrow();
	glUniform3fv(m_uniformLightPos, 1, m_lightPos.v); glThrow();

	glActiveTexture(GL_TEXTURE0); glThrow();
	glBindTexture(GL_TEXTURE_2D, *m_texture); glThrow();
    glUniform1i(m_uniformTexture, 0); glThrow();

    glBindBuffer(GL_ARRAY_BUFFER, *m_geometry); glThrow();

    glVertexAttribPointer(m_attributePosition, 3, GL_FLOAT, GL_FALSE, m_geomStride, nullptr); glThrow();
    glVertexAttribPointer(m_attributeNormal, 3, GL_FLOAT, GL_FALSE, m_geomStride, static_cast<char*>(nullptr) + 3 * sizeof(float)); glThrow();
    glVertexAttribPointer(m_attributeTexCoord, 2, GL_FLOAT, GL_FALSE, m_geomStride, static_cast<char*>(nullptr) + 6 * sizeof(float)); glThrow();

    glEnableVertexAttribArray(m_attributePosition); glThrow();
    glEnableVertexAttribArray(m_attributeNormal); glThrow();
    glEnableVertexAttribArray(m_attributeTexCoord); glThrow();
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *m_indices); glThrow();
    glDrawElements(GL_TRIANGLES, m_numIndices, GL_UNSIGNED_SHORT, nullptr); glThrow();
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); glThrow();
    glDisableVertexAttribArray(m_attributeTexCoord); glThrow();
    glDisableVertexAttribArray(m_attributeNormal); glThrow();
    glDisableVertexAttribArray(m_attributePosition); glThrow();
    glBindBuffer(GL_ARRAY_BUFFER, 0); glThrow();

    glBindTexture(GL_TEXTURE_2D, 0); glThrow();
}

void renderer_sphere::destroy() {
	m_attributePosition = -1;
	m_attributeNormal = -1;
	m_attributeTexCoord = -1;
	m_uniformMV = -1;
	m_uniformMVP = -1;
	m_uniformTexture = -1;
	m_uniformLightPos = -1;
	m_program = nullptr;
	m_geometry = nullptr;
	m_geomStride = 0;
	m_indices = nullptr;
	m_numVertices = 0;
	m_numIndices = 0;
	m_matModel.setIdentity();
	m_matView.setIdentity();
	m_matProjection.setIdentity();
	m_texture = nullptr;
	m_lightPos.set(0, 0, 0);
	m_time = 0;
}
