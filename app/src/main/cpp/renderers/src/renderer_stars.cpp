#include "renderer_stars.h"

#include <random>

#include "assets_util.h"

program_ptr m_program;
	buffer_ptr m_geometry;
	unsigned m_numVertices;
	unsigned m_numIndices;
	GLsizei m_geomStride;
	GLint m_uniformMVP;
	GLint m_uniformTime;
	GLint m_attributePosition;
	GLint m_attributeSize;
	GLint m_attributeColor;
	GLint m_attributeBlink;

renderer_stars::renderer_stars() :
	renderer{},
	m_program{},
	m_geometry{},
	m_indices{},
	m_numVertices{},
	m_numIndices{},
	m_geomStride{},
	m_uniformMVP{-1},
	m_uniformTime{-1},
	m_attributePosition{-1},
	m_attributeSize{-1},
	m_attributeColor{-1},
	m_attributeBlink{-1},
	m_time{},
	m_matModel{},
	m_matView{},
	m_matProjection{} {
}

renderer_stars::~renderer_stars() {
}

void renderer_stars::create(void* userData) {
#ifdef ANDROID
	m_program = link_program(load_text_asset(static_cast<AAssetManager*>(userData), std::string("shaders/stars_vert")),
							 load_text_asset(static_cast<AAssetManager*>(userData), std::string("shaders/stars_frag")));
#else //ANDROID
	m_program = link_program(load_text_file(std::string("assets/shaders/stars_vert")), load_text_file(std::string("assets/shaders/stars_frag")));
#endif //ANDROID

	m_uniformTime = get_uniform_location(m_program, "u_time");
	m_uniformMVP = get_uniform_location(m_program, "u_mvp");
	m_attributePosition = get_attribute_location(m_program, "a_position");
	m_attributeSize = get_attribute_location(m_program, "a_size");
	m_attributeColor = get_attribute_location(m_program, "a_color");
	m_attributeBlink = get_attribute_location(m_program, "a_blink");

	struct Attribs {
		vec3 position;
		float size;
		vec3 color;
		float blink;
	};

	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<> disPos(-1.0f, 1.0f);
	std::uniform_real_distribution<> disSize(30.0f, 100.0f);
	std::uniform_real_distribution<> disColR(0.8f, 1.0f);
	std::uniform_real_distribution<> disColB(0.8f, 1.0f);
	std::uniform_real_distribution<> disBl(0.2f, 2.0f);

	std::vector<Attribs> geometry;
	for (int i = 0; i < 10000; ++i) {
		Attribs attribs;

		attribs.position.x = disPos(gen);
		attribs.position.y = disPos(gen);
		attribs.position.z = disPos(gen);

		attribs.size = disSize(gen);

		attribs.color.r = disColR(gen);
		attribs.color.g = 1;
		attribs.color.b = disColB(gen);

		attribs.blink = disBl(gen);

		geometry.push_back(attribs);
	}


	m_geometry = make_array_buffer(geometry.size() * sizeof(Attribs), geometry.data(), GL_STATIC_DRAW);
	m_numVertices = geometry.size();
	m_geomStride = sizeof(Attribs);

	std::vector<GLushort> indices;

	for (unsigned i = 0; i < m_numVertices; ++i) {
		indices.push_back(i);
	}

	m_indices = make_element_array_buffer(indices.size() * sizeof(GLushort), indices.data(), GL_STATIC_DRAW);
	m_numIndices = indices.size();

	glEnable(GL_CULL_FACE); glThrow();
	glCullFace(GL_BACK); glThrow();
	glFrontFace(GL_CCW); glThrow();
	glEnable(GL_DEPTH_TEST); glThrow();
	glClearColor(0.4, 0.4, 0.4, 0.0); glThrow();
	glEnable(GL_BLEND); glThrow();
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); glThrow();
}

void renderer_stars::resize(unsigned width, unsigned height) {
	glViewport(0, 0, width, height); glThrow();

	const float aspect = static_cast<float>(width) / height;

	m_matProjection.perspective(deg_to_rad(60), aspect, 0.1f, 5.0f);
}

void renderer_stars::draw() {

	m_time += 16.0f/1000;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); glThrow();

	glUseProgram(*m_program); glThrow();

	glUniform1f(m_uniformTime, m_time); glThrow();
	glUniformMatrix4fv(m_uniformMVP, 1, GL_FALSE, (m_matProjection * m_matView * m_matModel).m); glThrow();

    glBindBuffer(GL_ARRAY_BUFFER, *m_geometry); glThrow();

    glVertexAttribPointer(m_attributePosition, 3, GL_FLOAT, GL_FALSE, 0, nullptr); glThrow();
    glVertexAttribPointer(m_attributeSize, 1, GL_FLOAT, GL_FALSE, m_geomStride, static_cast<char*>(nullptr) + 3 * sizeof(float)); glThrow();
    glVertexAttribPointer(m_attributeColor, 3, GL_FLOAT, GL_FALSE, m_geomStride, static_cast<char*>(nullptr) + 4 * sizeof(float)); glThrow();
    glVertexAttribPointer(m_attributeBlink, 1, GL_FLOAT, GL_FALSE, m_geomStride, static_cast<char*>(nullptr) + 7 * sizeof(float)); glThrow();

    glEnableVertexAttribArray(m_attributePosition); glThrow();
    glEnableVertexAttribArray(m_attributeSize); glThrow();
    glEnableVertexAttribArray(m_attributeColor); glThrow();
    glEnableVertexAttribArray(m_attributeBlink); glThrow();
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *m_indices); glThrow();
    glDrawElements(GL_POINTS, m_numIndices, GL_UNSIGNED_SHORT, nullptr); glThrow();
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); glThrow();
    glDisableVertexAttribArray(m_attributePosition); glThrow();
    glDisableVertexAttribArray(m_attributeSize); glThrow();
    glDisableVertexAttribArray(m_attributeColor); glThrow();
    glDisableVertexAttribArray(m_attributeBlink); glThrow();

    glBindBuffer(GL_ARRAY_BUFFER, 0); glThrow();
}

void renderer_stars::destroy() {
	m_program = nullptr;
	m_geometry = nullptr;
	m_indices = nullptr;
	m_numVertices = 0;
	m_numIndices = 0;
	m_geomStride = 0;
	m_uniformMVP = -1;
	m_uniformTime = -1;
	m_attributePosition = -1;
	m_attributeSize = -1;
	m_attributeColor = -1;
	m_attributeBlink = -1;
	m_time = 0;
	m_matModel.setIdentity();
	m_matView.setIdentity();
	m_matProjection.setIdentity();
}
