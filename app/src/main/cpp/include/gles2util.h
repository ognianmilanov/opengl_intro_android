#ifndef INCLUDE_GLES2UTIL_H_
#define INCLUDE_GLES2UTIL_H_

#include <stdexcept>
#include <memory>
#include <string>

#include <GLES2/gl2.h>

class gl_error : public std::runtime_error {
public:
	inline gl_error(GLenum code) : std::runtime_error{to_string(code)}, m_code{code} {}

	inline GLenum code() const {
		return m_code;
	}

private:

	static const char* to_string(GLenum code) {
		switch (code) {
		case GL_NO_ERROR: return "GL_NO_ERROR";
		case GL_INVALID_ENUM: return "GL_INVALID_ENUM";
		case GL_INVALID_VALUE: return "GL_INVALID_VALUE";
		case GL_INVALID_OPERATION: return "GL_INVALID_OPERATION";
		case GL_INVALID_FRAMEBUFFER_OPERATION: return "GL_INVALID_FRAMEBUFFER_OPERATION";
		case GL_OUT_OF_MEMORY: return "GL_OUT_OF_MEMORY";
		default: throw std::invalid_argument("Not a GL error code");
		};
	};

	const GLenum m_code;
};

inline void glThrow() {
	auto code = glGetError();
	if (code != GL_NO_ERROR) {
		throw gl_error{code};
	}
}

template<typename DestructorType, DestructorType* Destructor, typename ValidatorType, ValidatorType* Validator>
class gl_handle {
public:

	inline explicit gl_handle(GLuint handle) : m_handle{validate(handle, Validator)} {}

	gl_handle(const gl_handle&) = delete;
	gl_handle& operator=(const gl_handle&) = delete;

	inline gl_handle(gl_handle&& other) noexcept : m_handle{0} {
		std::swap(m_handle, other.m_handle);
	}

	inline gl_handle& operator=(gl_handle&& other) noexcept {
		m_handle = other.m_handle;
		other.m_handle = 0;
		return *this;
	}

	inline ~gl_handle() {
		if (m_handle) {
			destroy(Destructor);
			glGetError();
		}
	}

	inline operator GLuint() const noexcept {
		return m_handle;
	}

private:

	inline static GLuint validate(GLuint handle, GLboolean(*validator)(GLuint) __attribute__((pcs("aapcs")))) {
		if (!validator) return handle;
		const auto valid = validator(handle);
		glGetError();
		if (!valid) {
			throw std::runtime_error("Wrong object type");
		}
		return handle;
	}

	inline void destroy(void(*destructor)(GLsizei, const GLuint*) __attribute__((pcs("aapcs")))) {
		destructor(1, &m_handle);
	}

	inline void destroy(void(*destructor)(GLuint) __attribute__((pcs("aapcs")))) {
		destructor(m_handle);
	}

	GLuint m_handle;
};

using shader_t = gl_handle<decltype(glDeleteShader), &glDeleteShader, decltype(glIsShader), glIsShader>;
using program_t = gl_handle<decltype(glDeleteProgram), &glDeleteProgram, decltype(glIsProgram), glIsProgram>;
using texture_t = gl_handle<decltype(glDeleteTextures), &glDeleteTextures, decltype(glIsTexture), nullptr>;
using buffer_t = gl_handle<decltype(glDeleteBuffers), &glDeleteBuffers, decltype(glIsBuffer), nullptr>;
using framebuffer_t = gl_handle<decltype(glDeleteFramebuffers), &glDeleteFramebuffers, decltype(glIsFramebuffer), &glIsFramebuffer>;
using renderbuffer_t = gl_handle<decltype(glDeleteRenderbuffers), &glDeleteRenderbuffers, decltype(glIsRenderbuffer), &glIsRenderbuffer>;

using shader_ptr = std::shared_ptr<shader_t>;
using program_ptr = std::shared_ptr<program_t>;
using texture_ptr = std::shared_ptr<texture_t>;
using buffer_ptr = std::shared_ptr<buffer_t>;
using framebuffer_ptr = std::shared_ptr<framebuffer_t>;
using renderbuffer_ptr = std::shared_ptr<renderbuffer_t>;

using shader_wptr = std::weak_ptr<shader_t>;
using program_wptr = std::weak_ptr<program_t>;
using texture_wptr = std::weak_ptr<texture_t>;
using buffer_wptr = std::weak_ptr<buffer_t>;
using framebuffer_wptr = std::weak_ptr<framebuffer_t>;
using renderbuffer_wptr = std::weak_ptr<renderbuffer_t>;

shader_ptr compile_shader(const std::string& source, GLenum type);

program_ptr link_program(const shader_ptr& vertex, const shader_ptr& fragmet);

program_ptr link_program(const std::string& vertexSource, const std::string& fragmetSource);

GLint get_attribute_location(const program_ptr& program, const std::string& name);

GLint get_uniform_location(const program_ptr& program, const std::string& name);

buffer_ptr make_array_buffer(GLsizeiptr size, const GLvoid* data, GLenum usage);

buffer_ptr make_element_array_buffer(GLsizeiptr size, const GLvoid* data, GLenum usage);

texture_ptr make_texture2d(GLsizei width, GLsizei height, GLenum format, const GLvoid* data);

#endif /* INCLUDE_GLES2UTIL_H_ */
