#ifndef INCLUDE_RENDERER_H_
#define INCLUDE_RENDERER_H_

class renderer {
public:

	virtual ~renderer() {}

	virtual void create(void* userData = nullptr) = 0;

	virtual void resize(unsigned width, unsigned height) = 0;

	virtual void draw() = 0;

	virtual void destroy() = 0;

};

#endif /* INCLUDE_RENDERER_H_ */
