#ifndef INCLUDE_ASSETS_UTIL_H_
#define INCLUDE_ASSETS_UTIL_H_

#include "gles2util.h"

#ifdef ANDROID
#include <android/asset_manager_jni.h>
#endif //ANDROID

std::string load_text_file(const std::string& path);

uint8_t* load_binary_file(const std::string& path, std::size_t& size);

uint8_t* decode_png(const uint8_t* data, std::size_t size, unsigned& width, unsigned& height, GLint& format);

texture_ptr load_png_texture(const std::string& path, unsigned& width, unsigned& height, GLint& format);

#ifdef ANDROID
std::string load_text_asset(AAssetManager* assets, const std::string& path);

uint8_t* load_binary_asset(AAssetManager* assets, const std::string& path, std::size_t& size);

texture_ptr load_png_texture(AAssetManager* assets, const std::string& path, unsigned& width, unsigned& height, GLint& format);
#endif //ANDROID

#endif /* INCLUDE_ASSETS_UTIL_H_ */
