#ifndef OPENGL_INTRO_LOGGING_H
#define OPENGL_INTRO_LOGGING_H

#include <android/log.h>

#define LOG_TAG "OpenGL_intro"
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN, LOG_TAG, __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)

#define LINEMARK LOGD("Linemark %s %s:%d", __FUNCTION__, __FILE__, __LINE__);

#endif //OPENGL_INTRO_LOGGING_H
