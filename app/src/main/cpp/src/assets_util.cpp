#include "assets_util.h"

#include <cstring>
#include <fstream>

#include <png.h>

std::string load_text_file(const std::string& path) {
	std::ifstream stream(path);
	std::string content;
	stream.seekg(0, std::ios::end);
	content.reserve(stream.tellg());
	stream.seekg(0, std::ios::beg);
	content.assign((std::istreambuf_iterator<char>{stream}), std::istreambuf_iterator<char>{});
	return content;
}

uint8_t* load_binary_file(const std::string& path, std::size_t& size) {
    std::ifstream stream(path, std::ios::binary | std::ios::ate);
    size = stream.tellg();
    auto mem = new uint8_t[size];
    stream.seekg(0, std::ios::beg);
    stream.read(reinterpret_cast<char*>(mem), size);
    return mem;
}

struct PngIO {
    const uint8_t* data;
    std::size_t remaining;
};

static void readPngFunc(png_structp png, png_bytep data, png_size_t size) {
    auto io = static_cast<PngIO*>(png_get_io_ptr(png));
    if (size > io->remaining) {
        png_error(png, "EOF");
        return;
    }
    std::memcpy(data, io->data, size);
    io->data += size;
    io->remaining -= size;
}

uint8_t* decode_png(const uint8_t* data, std::size_t size, unsigned& width, unsigned& height, GLint& format) {
	std::string errorMsg;
	png_byte* content = nullptr;
	png_structp readStruct = nullptr;
	png_infop info = nullptr;
	png_infop endInfo = nullptr;
	PngIO memIO;
	int bitDepth, colorType, rowSize;
	png_byte** rows = nullptr;
	png_uint_32 pngWidth{}, pngHeight{};

	if (png_sig_cmp(const_cast<png_bytep>(data), 0, 8)) {
		errorMsg = std::string("Not a PNG compressed image");
		goto error;
	}

	readStruct = png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
	if (!readStruct) {
		errorMsg = std::string("png_create_read_struct failed");
		goto error;
	}

	info = png_create_info_struct(readStruct);
	if (!info) {
		errorMsg = std::string("png_create_info_struct failed");
		goto error;
	}

	endInfo = png_create_info_struct(readStruct);
	if (!endInfo) {
		errorMsg = std::string("png_create_info_struct failed");
		goto error;
	}

	if (setjmp(png_jmpbuf(readStruct))) {
		errorMsg = std::string("Internal error\n");
		goto error;
	}

	memIO = {data + 8, size};
	png_set_read_fn(readStruct, &memIO, readPngFunc);
	png_set_sig_bytes(readStruct, 8);
	png_read_info(readStruct, info);
	png_get_IHDR(readStruct, info, &pngWidth, &pngHeight, &bitDepth, &colorType, nullptr, nullptr, nullptr);
	width = pngWidth;
	height = pngHeight;

	if (bitDepth != 8) {
		errorMsg = std::string("Bit depth != 8");
		goto error;
	}

	switch (colorType) {
		case PNG_COLOR_TYPE_RGB:
			format = GL_RGB;
			break;
		case PNG_COLOR_TYPE_RGB_ALPHA:
			format = GL_RGBA;
			break;
		default:
			errorMsg = std::string("Unsupported color type");
			goto error;
	}

	png_read_update_info(readStruct, info);
	rowSize = png_get_rowbytes(readStruct, info);
	rowSize += 3 - ((rowSize -1) % 4);
	content = new png_byte[rowSize * height * sizeof(png_byte) + 15];
	if (content == nullptr) {
		errorMsg = std::string("Out of memory");
		goto error;
	}

	rows = new png_byte*[height * sizeof(png_byte*)];
	if (rows == nullptr) {
		errorMsg = std::string("Out of memory");
		goto error;
	}

	for (unsigned int i = 0; i < height; ++i) {
		rows[height - 1 - i] = content + i * rowSize;
	}

	png_read_image(readStruct, rows);

	png_destroy_read_struct(&readStruct, &info, &endInfo);
	delete [] rows;

	return content;

	error:

		png_destroy_read_struct(&readStruct, &info, &endInfo);
		delete [] rows;
		delete [] content;

		throw std::runtime_error(errorMsg);
}

texture_ptr load_png_texture(const std::string& path, unsigned& width, unsigned& height, GLint& format) {
	std::size_t size{};
	auto encoded = load_binary_file(path, size);
	auto decoded = decode_png(encoded, size, width, height, format);
	auto texture = make_texture2d(width, height, format, decoded);
	delete [] decoded;
	delete [] encoded;
	return texture;
}

#ifdef ANDROID

std::string load_text_asset(AAssetManager* assets, const std::string& path) {
	std::string errorMsg;
	AAsset* asset{};
	std::string content;

	if (!assets) {
		errorMsg = "No asset manager";
		goto error;
	}

	asset = AAssetManager_open(assets, path.c_str(), AASSET_MODE_BUFFER);
	if (!asset) {
		errorMsg = "Unable to open asset " + path;
		goto error;
	}

	{
		off_t size = AAsset_getLength(asset);
		int bytesRead{};
		char buf[size + 1];
		while (bytesRead < size) {
			auto bread = AAsset_read(asset, buf + bytesRead, size - bytesRead);
			if (bread == 0) break;
			if (bread < 0) {
				errorMsg = "Error reading asset " + path;
				goto error;
			}
			bytesRead += bread;
		}
		buf[bytesRead] = '\0';
		content = buf;
	}

	AAsset_close(asset);

	return content;

	error:

	if (asset) {
		AAsset_close(asset);
	}

	throw std::runtime_error(errorMsg);
}

uint8_t* load_binary_asset(AAssetManager* assets, const std::string& path, std::size_t& size) {
	std::string errorMsg;
	AAsset* asset{};
	uint8_t* content = nullptr;

	if (!assets) {
		errorMsg = "No asset manager";
		goto error;
	}

	asset = AAssetManager_open(assets, path.c_str(), AASSET_MODE_BUFFER);
	if (!asset) {
		errorMsg = "Unable to open asset " + path;
		goto error;
	}

	{
		size = AAsset_getLength(asset);
		unsigned bytesRead{};
		content = new uint8_t[size];
		while (bytesRead < size) {
			auto bread = AAsset_read(asset, content + bytesRead, size - bytesRead);
			if (bread == 0) break;
			if (bread < 0) {
				errorMsg = "Error reading asset " + path;
				goto error;
			}
			bytesRead += bread;
		}
	}

	AAsset_close(asset);

	return content;

	error:

	if (asset) {
		AAsset_close(asset);
	}

	delete [] content;

	throw std::runtime_error(errorMsg);
}

texture_ptr load_png_texture(AAssetManager* assets, const std::string& path, unsigned& width, unsigned& height, GLint& format) {
	std::size_t size{};
	auto encoded = load_binary_asset(assets, path, size);
	auto decoded = decode_png(encoded, size, width, height, format);
	auto texture = make_texture2d(width, height, format, decoded);
	delete [] decoded;
	delete [] encoded;
	return texture;
}

#endif //ANDROID