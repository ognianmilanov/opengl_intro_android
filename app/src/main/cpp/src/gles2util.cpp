#include "gles2util.h"

#include <fstream>
#include <streambuf>
#include <vector>

/*
 * Shader
 */

static shader_ptr new_shader(GLenum type) {
	switch (type) {
	case GL_VERTEX_SHADER:
	case GL_FRAGMENT_SHADER:
		break;
	default:
		throw std::runtime_error("Wrong shader type");
	}
	auto shader = glCreateShader(type); glThrow();
	return std::make_shared<shader_t>(shader);
}

static bool get_compile_status(const shader_ptr& shader) {
    GLint ret{};
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &ret); glThrow();
    return !!ret;
}

static std::string get_info_log(const shader_ptr& shader) {
	GLint len{};
	glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &len); glThrow();
    std::vector<GLchar> log(len);
    glGetShaderInfoLog(*shader, len, &len, &log[0]); glThrow();
    return std::string(&log[0], len);
}

shader_ptr compile_shader(const std::string& source, GLenum type) {
	auto shader = new_shader(type);
	const GLchar* arr[] = { source.c_str() };
	glShaderSource(*shader, 1, arr, nullptr); glThrow();
	glCompileShader(*shader); glThrow();
	if (!get_compile_status(shader)) {
		throw std::runtime_error{"Shader compilation error:\n" + get_info_log(shader)};
	}
	return shader;
}

/*
 * Program
 */

static program_ptr new_program() {
	auto program = glCreateProgram(); glThrow();
	return std::make_shared<program_t>(program);
}

static std::string get_info_log(const program_ptr& program) {
	GLint len{};
	glGetProgramiv(*program, GL_INFO_LOG_LENGTH, &len); glThrow();
    std::vector<GLchar> log(len);
    glGetProgramInfoLog(*program, len, &len, &log[0]); glThrow();
    return std::string(&log[0], len);
}

bool get_link_status(const program_ptr& program) {
    GLint ret{};
    glGetProgramiv(*program, GL_LINK_STATUS, &ret); glThrow();
    return !!ret;
}

bool get_validate_status(const program_ptr& program) {
    GLint ret{};
    glGetProgramiv(*program, GL_VALIDATE_STATUS, &ret); glThrow();
    return !!ret;
}

program_ptr link_program(const shader_ptr& vertex, const shader_ptr& fragmet) {
	auto program = new_program();
	glAttachShader(*program, *vertex); glThrow();
	glAttachShader(*program, *fragmet); glThrow();
	glLinkProgram(*program); glThrow();
	if (!get_link_status(program)) {
		throw std::runtime_error{"Program link error:\n" + get_info_log(program)};
	}
	glValidateProgram(*program); glThrow();
	if (!get_validate_status(program)) {
		throw std::runtime_error{"Program validate error:\n" + get_info_log(program)};
	}
	return program;
}

program_ptr link_program(const std::string& vertexSource, const std::string& fragmetSource) {
	return link_program(
			compile_shader(vertexSource, GL_VERTEX_SHADER),
			compile_shader(fragmetSource, GL_FRAGMENT_SHADER));
}

GLint get_attribute_location(const program_ptr& program, const std::string& name) {
    auto ret = glGetAttribLocation(*program, name.c_str()); glThrow();
    if (ret == -1) throw std::runtime_error("Attribute not found: " + name);
    return ret;
}

GLint get_uniform_location(const program_ptr& program, const std::string& name) {
    auto ret = glGetUniformLocation(*program, name.c_str()); glThrow();
    if (ret == -1) throw std::runtime_error("Uniform not found: " + name);
    return ret;
}

/*
 * Buffer
 */

static buffer_ptr new_buffer() {
	GLuint buffer{};
	glGenBuffers(1, &buffer); glThrow();
	return std::make_shared<buffer_t>(buffer);
}

buffer_ptr make_array_buffer(GLsizeiptr size, const GLvoid* data, GLenum usage) {
	switch (usage) {
	case GL_STATIC_DRAW:
	case GL_DYNAMIC_DRAW:
		break;
	default:
		throw std::runtime_error("Wrong array buffer usage type");
	}
	auto buffer = new_buffer();
	glBindBuffer(GL_ARRAY_BUFFER, *buffer); glThrow();
	glBufferData(GL_ARRAY_BUFFER, size, data, usage); glThrow();
	glBindBuffer(GL_ARRAY_BUFFER, 0); glThrow();
	return buffer;
}

buffer_ptr make_element_array_buffer(GLsizeiptr size, const GLvoid* data, GLenum usage) {
	switch (usage) {
	case GL_STATIC_DRAW:
	case GL_DYNAMIC_DRAW:
		break;
	default:
		throw std::runtime_error("Wrong element array buffer usage type");
	}
	auto buffer = new_buffer();
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *buffer); glThrow();
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, data, usage); glThrow();
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); glThrow();
	return buffer;
}

/*
 * Texture
 */

static texture_ptr new_texture2d() {
	GLuint texture{};
	glGenTextures(1, &texture); glThrow();
	return std::make_shared<texture_t>(texture);
}

static GLint getInternalFormat(GLenum format) {
    switch (format) {
        case GL_RGB565: return GL_RGB;
        default: return (GLint) format;
    }
}

static GLint getFormat(GLenum format) {
    switch (format) {
        case GL_RGB565: return GL_RGB;
        default: return (GLint) format;
    }
}

static GLenum getDataType(GLenum format) {
    switch (format) {
        case GL_RGB565: return GL_UNSIGNED_SHORT_5_6_5;
        default: return GL_UNSIGNED_BYTE;
    }
}

texture_ptr make_texture2d(GLsizei width, GLsizei height, GLenum format, const GLvoid* data) {
	texture_ptr texture = new_texture2d();
	glBindTexture(GL_TEXTURE_2D, *texture); glThrow();

    glTexImage2D(GL_TEXTURE_2D, 0, getInternalFormat(format), width, height, 0, getFormat(format), getDataType(format), data); glThrow();
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); glThrow();
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); glThrow();
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); glThrow();
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); glThrow();

	glBindTexture(GL_TEXTURE_2D, 0); glThrow();
	return texture;
}
