APP_ABI := armeabi-v7a x86
NDK_TOOLCHAIN_VERSION := clang
APP_STL := c++_static
APP_CPPFLAGS += -std=c++11 -fexceptions -frtti -pthread -Wall -Wextra -Weffc++
APP_PLATFORM := android-22
APP_OPTIM := debug